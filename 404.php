<?php
/**
 * 404 Page Template
 *
 * @package     GroffTech\PhoenixTimber
 * @since       1.0.0
 * @author      Brett Groff
 * @link        https://grofftech.net
 * @license     GNU General Public License 2.0+
 */

namespace GroffTech\PhoenixTimber;

$context = $timber->get_context();
$timber->render( '404.twig', $context );
