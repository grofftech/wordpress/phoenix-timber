<?php
/**
 * Taxonomy Tests.
 *
 * @package     GroffTech\PhoenixTimber\Tests\Unit\Helpers
 * @since       1.0.0
 * @author      Brett Groff
 * @link        https://grofftech.net
 * @license     GNU General Public License 2.0+
 */

namespace GroffTech\PhoenixTimber\Tests\Unit\Helpers;

use Mockery;
use Brain\Monkey\Functions;
use GroffTech\PhoenixTimber\Helpers\StringHelper;
use GroffTech\PhoenixTimber\Helpers\UrlHelper;
use GroffTech\PhoenixTimber\Tests\Unit\TestCase;

/**
 * Taxonomy Tests class.
 */
class UrlHelperTests extends TestCase {

    /**
     * Url Helper object.
     *
     * @var UrlHelper
     */
    private $url_helper;

    /**
     * Runs setup code before each test.
     *
     * @since 1.0.0
     *
     * @return void
     */
    protected function setUp(): void {
        parent::setup();

        $string_helper_mock = Mockery::mock( StringHelper::class )
            ->makePartial();

        $this->url_helper = new UrlHelper( $string_helper_mock );
    }

    /**
     * Test get_page_from_url() returns page number with slash at the end.
     */
    public function test_page_is_returned_when_in_url_with_slash_at_end() {
        $url = 'http://starwarsisawesome.com/shop/page/1/';
        $expected_page_number = '1';

        $actual_page_number = $this->url_helper->get_page_from_url( $url );

        $this->assertSame( $expected_page_number, $actual_page_number );
    }

    /**
     * Test get_page_from_url() returns page number with no slash at the end.
     */
    public function test_page_is_returned_when_in_url_with_no_slash_at_end() {
        $url = 'http://starwarsisawesome.com/shop/page/2';
        $expected_page_number = '2';

        $actual_page_number = $this->url_helper->get_page_from_url( $url );

        $this->assertSame( $expected_page_number, $actual_page_number );
    }

    /**
     * Test get_query_string_as-array() returns array for one query parameter.
     */
    public function test_query_string_is_array_when_one_parameter() {
        $query_string = 'starwars=awesome';

        $arr = $this->url_helper->get_query_string_as_array( $query_string );

        $this->assertArrayHasKey( 'starwars', $arr );
        $this->assertEquals( 'awesome', $arr['starwars'] );
    }

    /**
     * Test get_query_string_as_array() returns array with several query parameters.
     */
    public function test_query_string_is_array_when_multiple_parameters() {
        $query_string = 'starwars=awesome&luke=skywalker&darthvader=anakin';

        $arr = $this->url_helper->get_query_string_as_array( $query_string );

        $this->assertArrayHasKey( 'starwars', $arr );
        $this->assertEquals( 'awesome', $arr['starwars'] );
        $this->assertArrayHasKey( 'luke', $arr );
        $this->assertEquals( 'skywalker', $arr['luke'] );
        $this->assertArrayHasKey( 'darthvader', $arr );
        $this->assertEquals( 'anakin', $arr['darthvader'] );
    }
}
