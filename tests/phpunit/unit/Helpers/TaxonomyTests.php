<?php
/**
 * Taxonomy Tests.
 *
 * @package     GroffTech\PhoenixTimber\Tests\Unit\Helpers
 * @since       1.0.0
 * @author      Brett Groff
 * @link        https://grofftech.net
 * @license     GNU General Public License 2.0+
 */

namespace GroffTech\PhoenixTimber\Tests\Unit\Helpers;

use Mockery;
use Brain\Monkey\Functions;
use GroffTech\PhoenixTimber\Helpers\TaxonomyHelper;
use GroffTech\PhoenixTimber\Tests\Unit\TestCase;

/**
 * Taxonomy Tests class.
 */
class TaxonomyTests extends TestCase {
    /**
     * Test get_taxonomy_terms() returns taxonomies with name arg and configured defaults.
     */
    public function test_should_return_taxonomy_with_defaults_when_only_name_is_provided() {
        $defaults = array(
            'taxonomy' => 'test',
            'hide_empty' => false,
            'order_by' => 'name',
        );

        $taxonomy_mock = Mockery::mock( TaxonomyHelper::class )
            ->makePartial();

        Functions\when( 'get_terms' )->returnArg();

        $this->assertSame( $defaults, $taxonomy_mock->get_taxonomy_terms( 'test' ) );
    }

    /**
     * Test get_taxonomy_terms() returns taxonomies with only options arg.
     */
    public function test_should_return_taxonomy_when_only_options_is_provided() {
        $options = array(
            'taxonomy' => 'test',
            'hide_empty' => false,
            'order_by' => 'name',
        );

        $taxonomy_mock = Mockery::mock( TaxonomyHelper::class )
            ->makePartial();

        Functions\when( 'get_terms' )->returnArg();

        $this->assertSame( $options, $taxonomy_mock->get_taxonomy_terms( '', $options ) );
    }

    /**
     * Test get_taxonomy_terms() errors when no taxonomy key is provided with options arg.
     */
    public function test_should_error_when_only_options_is_provided_with_no_taxonomy_key() {
        $options = array(
            'hide_empty' => false,
            'order_by' => 'name',
        );
        $taxonomy_mock = Mockery::mock( TaxonomyHelper::class )->makePartial();
        Functions\when( 'get_terms' )->returnArg();

        $this->expectException( \Exception::class );
        $taxonomy_mock->get_taxonomy_terms( '', $options );
    }
}
