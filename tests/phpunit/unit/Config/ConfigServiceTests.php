<?php
/**
 * Config Service Tests
 *
 * @package     GroffTech\PhoenixTimber\Tests\Unit\Config
 * @since       1.0.0
 * @author      Brett Groff
 * @link        https://grofftech.net
 * @license     GNU General Public License 2.0+
 */

namespace GroffTech\PhoenixTimber\Tests\Unit\Config;

use GroffTech\PhoenixTimber\Config\ConfigService;
use GroffTech\PhoenixTimber\Tests\Unit\TestCase;

/**
 * Unit tests for Config Service class
 */
class ConfigServiceTests extends TestCase {
    /**
     * The config service object.
     *
     * @var ConfigService
     */
    private $config_service;

    /**
     * An array of configuration file paths.
     *
     * @var array
     */
    private $config_files;

    /**
	 * Prepares the test environment before each test.
	 */
    protected function setUp(): void {
        parent::setUp();
        $this->config_service = new ConfigService();

        $this->config_files = array(
            PHOENIX_TIMBER_TESTS_DIRECTORY . '/Config/Configs/LukeSkywalker.php',
            PHOENIX_TIMBER_TESTS_DIRECTORY . '/Config/Configs/HanSolo.php',
            PHOENIX_TIMBER_TESTS_DIRECTORY . '/Config/Configs/C3PO.php',
            PHOENIX_TIMBER_TESTS_DIRECTORY . '/Config/Configs/Tatooine.php',
        );
    }

    /**
	 * Cleans up the test environment after each test.
	 */
    protected function tearDown(): void {
        parent::tearDown();
        $this->config_service = null;
        $this->config_files = array();
    }

    /**
     * Test load_configuration() loads the configuration as is.
     */
    public function test_should_load_configuration_without_defaults() {
        $config_file = PHOENIX_TIMBER_TESTS_DIRECTORY . '/Config/Configs/LukeSkywalker.php';

        $this->config_service->load_configuration( $config_file );
        $configuration =
            $this->config_service->get_configuration( 'character.luke_skywalker' );

        $this->assertArrayHasKey( 'details', $configuration );
        $this->assertArrayHasKey( 'name', $configuration['details'] );
        $this->assertArrayHasKey( 'occupation', $configuration['details'] );
        $this->assertArrayNotHasKey( 'catch_phrase', $configuration['details'] );
        $this->assertArrayHasKey( 'hero_status', $configuration );
    }

    /**
     * Test load_configuration() saves the configuration with defaults merged.
     */
    public function test_should_load_configuration_with_defaults() {
        $config_file = PHOENIX_TIMBER_TESTS_DIRECTORY . '/Config/Configs/HanSolo.php';
        $defaults =
            current( include PHOENIX_TIMBER_TESTS_DIRECTORY . '/Config/Configs/DefaultsHanSolo.php' );

        $this->config_service->load_configuration( $config_file, $defaults );
        $configuration =
            $this->config_service->get_configuration( 'character.han_solo' );

        $this->assertArrayHasKey( 'details', $configuration );
        $this->assertArrayHasKey( 'name', $configuration['details'] );
        $this->assertArrayHasKey( 'occupation', $configuration['details'] );
        $this->assertArrayHasKey( 'catch_phrase', $configuration['details'] );

        $this->assertStringContainsString(
            'I\'ve got a bad feeling about this',
            $configuration['details']['catch_phrase']
        );

        $this->assertStringContainsString(
            'smuggler',
            $configuration['details']['occupation']
        );

        $this->assertArrayHasKey( 'hero_status', $configuration );
    }

    /**
     * Test get_configurations() returns all the stored configurations.
     */
    public function test_should_return_all_configurations() {
        foreach ( $this->config_files as $config ) {
            $this->config_service->load_configuration( $config );
        }
        $configurations = $this->config_service->get_configurations();

        $this->assertArrayHasKey( 'character.luke_skywalker', $configurations );
        $this->assertArrayHasKey( 'character.han_solo', $configurations );
        $this->assertArrayHasKey( 'character.c3po', $configurations );
        $this->assertArrayHasKey( 'planet.tatooine', $configurations );
    }

    /**
     * Test get_configuration_keys() returns all the keys.
     */
    public function test_should_return_all_keys() {
        foreach ( $this->config_files as $config ) {
            $this->config_service->load_configuration( $config );
        }
        $keys = $this->config_service->get_configuration_keys();

        $this->assertContainsEquals( 'character.luke_skywalker', $keys );
        $this->assertContainsEquals( 'character.han_solo', $keys );
        $this->assertContainsEquals( 'character.c3po', $keys );
        $this->assertContainsEquals( 'planet.tatooine', $keys );
    }

    /**
     * Test get_configuration_keys_starting_with() returns the correct keys.
     */
    public function test_should_return_keys_starting_with() {
        foreach ( $this->config_files as $config ) {
            $this->config_service->load_configuration( $config );
        }
        $keys = $this->config_service->get_configuration_keys_starting_with( 'character' );

        $this->assertContainsEquals( 'character.luke_skywalker', $keys );
        $this->assertContainsEquals( 'character.han_solo', $keys );
        $this->assertContainsEquals( 'character.c3po', $keys );
    }

    /**
     * Test get_configuration() returns the correct configuration.
     */
    public function test_should_return_specific_configuration() {
        $config_file = PHOENIX_TIMBER_TESTS_DIRECTORY . '/Config/Configs/LukeSkywalker.php';

        $this->config_service->load_configuration( $config_file );
        $configuration =
            $this->config_service->get_configuration( 'character.luke_skywalker' );

        $this->assertArrayHasKey( 'details', $configuration );
        $this->assertArrayHasKey( 'name', $configuration['details'] );
        $this->assertArrayHasKey( 'occupation', $configuration['details'] );
        $this->assertArrayNotHasKey( 'catch_phrase', $configuration['details'] );
        $this->assertArrayHasKey( 'hero_status', $configuration );
    }

    /**
     * Test get_configuration_parameters() returns the correct parameters.
     */
    public function test_should_return_configuration_parameters() {
        $config_file = PHOENIX_TIMBER_TESTS_DIRECTORY . '/Config/Configs/LukeSkywalker.php';

        $this->config_service->load_configuration( $config_file );
        $parameters = $this->config_service->get_configuration_parameters( 'character.luke_skywalker', 'details' );

        $this->assertArrayHasKey( 'name', $parameters );
        $this->assertArrayHasKey( 'occupation', $parameters );
    }

    /**
     * Test get_configuration_parameters() errors when key is not found.
     */
    public function test_should_return_error_when_no_configuration_parameter_found() {
        $config_file = PHOENIX_TIMBER_TESTS_DIRECTORY . '/Config/Configs/LukeSkywalker.php';

        $this->config_service->load_configuration( $config_file );

        $this->expectException( \Exception::class );
        $parameters = $this->config_service->get_configuration_parameters( 'character.luke_skywalker', 'jedi' );
    }
}
