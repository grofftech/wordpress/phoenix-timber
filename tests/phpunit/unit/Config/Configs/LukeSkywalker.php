<?php
/**
 * Test Configuration
 *
 * @package     GroffTech\PhoenixTimber\Tests\Unit\Config
 * @since       1.0.0
 * @author      Brett Groff
 * @link        https://grofftech.net
 * @license     GNU General Public License 2.0+
 */

namespace GroffTech\PhoenixTimber\Tests\Unit\Config;

return array(
    'character.luke_skywalker' => array(
        'details' => array(
            'name' => 'Luke Skywalker',
            'occupation' => 'Jedi Master',
        ),
        'hero_status' => true,
    ),
);
