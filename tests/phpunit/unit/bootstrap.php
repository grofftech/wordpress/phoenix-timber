<?php
/**
 * Bootstraps the theme's unit tests
 *
 * @package     GroffTech\PhoenixTimber\Tests\Unit
 * @since       1.0.0
 * @author      Brett Groff
 * @link        https://grofftech.net
 * @license     GNU General Public License 2.0+
 */

namespace GroffTech\PhoenixTimber\Tests\Unit;

define( 'PHOENIX_TIMBER_TESTS_DIRECTORY', __DIR__ );
define(
    'PHOENIX_TIMBER_THEME_DIRECTORY',
    dirname( dirname( dirname( __DIR__ ) ) )
);

$composer_autoload = PHOENIX_TIMBER_THEME_DIRECTORY . '/vendor/autoload.php';

if ( file_exists( $composer_autoload ) ) {
    require_once $composer_autoload;
}
