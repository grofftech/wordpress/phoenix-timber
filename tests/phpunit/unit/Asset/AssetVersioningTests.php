<?php
/**
 * Asset Versioning Tests
 *
 * @package     GroffTech\PhoenixTimber\Tests\Unit\Asset
 * @since       1.0.0
 * @author      Brett Groff
 * @link        https://grofftech.net
 * @license     GNU General Public License 2.0+
 */

namespace GroffTech\PhoenixTimber\Tests\Unit\Asset;

use Mockery;
use GroffTech\PhoenixTimber\Tests\Unit\TestCase;
use Brain\Monkey\Functions;

/**
 * Asset Versioning tests.
 */
class AssetVersioningTests extends TestCase {
    /**
     * Test get_theme_version() returns timestamp when in development/debug mode.
     */
    public function test_should_return_timestamp_when_in_development_mode() {
        $mock = Mockery::mock( 'GroffTech\PhoenixTimber\Asset\AssetVersioning' )->makePartial();
        $mock
            ->shouldReceive( 'get_asset_current_timestamp' )
            ->once()
            ->with( Mockery::any() )
            ->andReturn( '12345' );
        $mock->development_mode = true;
        Functions\when( 'get_stylesheet_directory' )->justReturn();

        $version = $mock->get_theme_version();

        $this->assertEquals( '12345', $version );
    }

    /**
     * Test should return theme version when development/debug mode is turned off.
     */
    public function test_should_return_theme_version_when_not_in_development_mode() {
        $mock = Mockery::mock( 'GroffTech\PhoenixTimber\Asset\AssetVersioning' )->makePartial();
        $mock->development_mode = false;
        $theme_mock = Mockery::mock( 'WP_Theme' );
        $theme_mock
            ->shouldReceive( 'get' )
            ->once()
            ->with( 'Version' )
            ->andReturn( '1.0.0' );
        Functions\when( 'wp_get_theme' )->justReturn( $theme_mock );

        $version = $mock->get_theme_version();

        $this->assertEquals( '1.0.0', $version );
    }
}
