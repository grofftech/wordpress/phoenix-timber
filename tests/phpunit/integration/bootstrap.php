<?php
/**
 * Bootstrap for integration tests
 *
 * @package     GroffTech\PhoenixTimber\Tests\Integration
 * @since       1.0.0
 * @author      Brett Groff
 * @link        https://grofftech.net
 * @license     GNU General Public License 2.0+
 */

namespace GroffTech\PhoenixTimber\Tests\Integration;
