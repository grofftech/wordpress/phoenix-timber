<?php
/**
 * Phoenix Timber
 *
 * @package     GroffTech\PhoenixTimber
 * @since       1.0.0
 * @author      Brett Groff
 * @link        https://grofftech.net
 * @license     GNU General Public License 2.0+
 */

namespace GroffTech\PhoenixTimber;

use GroffTech\PhoenixTimber\Dependencies\Auryn\Injector;
use GroffTech\PhoenixTimber\Admin\Notification\Notification;
use GroffTech\PhoenixTimber\Dependencies\Timber\Timber;

$theme_dir = \get_stylesheet_directory();
$theme_url = \get_stylesheet_directory_uri();

// Import and create the notification class.
require_once $theme_dir . '/lib/Admin/Notification/Notification.php';
$notification = new Notification();

// Composer.
$composer_autoload = "{$theme_dir}/vendor/autoload.php";
if ( file_exists( $composer_autoload ) ) {
    require_once $composer_autoload;
}

// Timber.
$timber = new Timber();
$timber::$locations = array(
    __DIR__ . '/views/admin',
);

// Theme.
global $phoenix_timber;
$phoenix_timber = new PhoenixTimber( $theme_dir, $theme_url, new Injector() );

// Bootstrap.
try {
    \add_action( 'after_setup_theme', array( $phoenix_timber, 'run' ) );
} catch ( \Throwable $e ) {
    $notification->show_error_message( 'Failed to activate the Phoenix Timber theme' );
}
