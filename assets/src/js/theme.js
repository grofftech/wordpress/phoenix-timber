let phoenixTimber = ( function( document ) {
    "use strict";

    //
    // Public Variables
    //

    let publicAPI = { };

    //
    // Public Methods
    //

    /**
     * Init
     */
    publicAPI.init = function() {
        console.log("Test");
    };

    return publicAPI;

} )( document );

phoenixTimber.init();