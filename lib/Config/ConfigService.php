<?php
/**
 * Configuration Service
 *
 * @package     GroffTech\PhoenixTimber\ConfigService
 * @since       1.0.0
 * @author      Brett Groff
 * @license     GNU-2.0+
 */

namespace GroffTech\PhoenixTimber\Config;

/**
 * Config Service class.
 */
class ConfigService {
    /**
     * An array of configurations that get added to
     * each time a configuration is loaded
     *
     * @var array
     */
    private $configurations = array();

    /**
     * Constructor.
     */
    public function __construct() {

    }

    /**
     * Loads a configuration file and saves it into the store.
     * Merges in a default configuration array if supplied.
     *
     * @since 1.0.0
     *
     * @param string $file_path The file path of the configuration file to load.
     * @param array  $defaults Optional. An array of default configurations.

     * @return void
     */
    public function load_configuration( $file_path, array $defaults = array() ) {
        list( $key, $config ) =
            $this->load_config_from_filesystem( $file_path );

        if ( $defaults ) {
            $config = $this->merge_with_defaults(
                $config,
                $defaults
            );
        }

        if ( array_key_exists('custom_fields', $config ) ) {
            $config = $this->clean_up_custom_fields_configuration( $config );
        }

        $this->save( $config, $key );
    }

    /**
     * Saves the configuration.
     *
     * @since 1.0.0
     *
     * @param array  $configuration The configuration.
     * @param string $key The key for the configuration.
     *
     * @return void
     */
    private function save( $configuration, $key ) {
        $this->configurations[ $key ] = $configuration;
    }

    /**
     * Gets all the configurations.
     *
     * @since 1.0.0
     *
     * @return array
     */
    public function get_configurations() {
        return $this->configurations;
    }

    /**
     * Gets all the keys from configurations.
     *
     * @since 1.0.0
     *
     * @return array
     */
    public function get_configuration_keys() {
        return array_keys( $this->configurations );
    }

    /**
     * Gets the keys from configurations in the store starting with
     * a specific word (e.g.) metabox.portfolio
     *
     * @since 1.0.0
     *
     * @param string $starts_with The word the key starts with, including the period.
     *
     * @return array
     */
    public function get_configuration_keys_starting_with( $starts_with ) {
        return array_filter(
            $this->get_configuration_keys(),
            function ( $key ) use ( $starts_with ) {
				return Helpers::string_starts_with( $key, $starts_with );
			}
        );
    }

    /**
     * Get a specific configuration.
     *
     * @since 1.0.0
     *
     * @param string $key The configuration key.
     *
     * @return array
     */
    public function get_configuration( $key ) {
        return $this->configurations[ $key ];
    }

    /**
     * Get options for a specific parameter in a configuration.
     *
     * @since 1.0.0
     *
     * @param string $key The configuration key.
     * @param string $parameter_key The configuration parameter.
     *
     * @throws \Exception If the parameter key does not exist for the configuration.
     *
     * @return array
     */
    public function get_configuration_parameters( $key, $parameter_key ) {
        $configuration = $this->get_configuration( $key );

        if ( ! array_key_exists( $parameter_key, $configuration ) ) {
            throw new \Exception(
                "Configuration for {$parameter_key} does not exist for {$key}"
            );
        }

        return $configuration[ $parameter_key ];
    }

    /**
     * Loads in the configuration file from the filesystem.
     *
     * @since 1.0.0
     *
     * @param string $file_path The file path of the configuration file to load.
     *
     * @return array
     */
    private function load_config_from_filesystem( $file_path ) {
        $configuration = include $file_path;

        return array(
            key( $configuration ),
            current( $configuration ),
        );
    }

    /**
     * Merges the configuration from the store with default configurations.
     *
     * @since 1.0.0
     *
     * @param array $configuration An array of configuration options.
     * @param array $defaults An array of default configuration options.

     * @return array
     */
    private function merge_with_defaults( array $configuration, array $defaults ) {
        return array_replace_recursive( $defaults, $configuration );
    }

    /**
    * Removes the custom fields key from the the default configuration
    * and then merges the default options back in, so custom configurations
    * have the options they need.
    *
    * @since 1.0.0
    *
    * @param array $config The configuration.
    *
    * @return array A merged array of custom fields options.
    */
    private function clean_up_custom_fields_configuration( array $config ) {
        $custom_fields = $config['custom_fields'];
        $defaults = array_shift( $custom_fields );

        foreach( $custom_fields as $key => $value ) {
            $custom_fields[$key] = array_merge($defaults, $value);
        }

        $config['custom_fields'] = $custom_fields;

        return $config;
    }
}
