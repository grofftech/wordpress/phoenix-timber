<?php
/**
 * Config Store Helpers
 *
 * @package     GroffTech\ConfigStore
 * @since       1.0.0
 * @author      Brett Groff
 * @license     GNU-2.0+
 */

namespace GroffTech\PhoenixTimber\Config;

/**
 * Helpers class.
 */
class Helpers {
    /**
     * Performs a search on a string starting with another string.
     *
     * @since 1.0.0
     *
     * @param string $haystack The string to search.
     * @param string $needle The string to search for.
     * @param string $encoding The encoding, optional. Default is UTF-8.
     *
     * @return bool Returns true if the string being searched starts with the string searching for, otherwise false.
     */
    public static function string_starts_with( $haystack, $needle, $encoding = 'UTF-8' ) {
        $needle_length = mb_strlen( $needle, $encoding );
        return mb_substr( $haystack, 0, $needle_length, $encoding ) === $needle;
    }
}
