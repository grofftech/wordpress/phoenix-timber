<?php
/**
 * Handles moving uploaded files
 *
 * @package     GroffTech\PhoenixTimber\File
 * @since       1.0.0
 * @author      Brett Groff
 * @link        https://grofftech.net
 * @license     GNU General Public License 2.0+
 */

namespace GroffTech\PhoenixTimber\File;

use GroffTech\PhoenixTimber\Helpers\FormHelper;
use GroffTech\PhoenixTimber\Service\Service;

/**
 * Upload class.
 */
class UploadFile extends Service {

    /**
     * Form Helper class
     *
     * @var FormHelper
     */
    private $form_helper;

    /**
     * Constructor
     *
     * @param FormHelper $form_helper The form helper object.
     */
    public function __construct( FormHelper $form_helper ) {
        $this->form_helper = $form_helper;
    }

    /**
     * Registers hooks.
     *
     * @since 1.0.0
     *
     * @return void
     */
    public function register_hooks() {
        \add_filter( 'wp_handle_upload_prefilter', array( $this, 'handle_pre_upload' ) );
        \add_filter( 'wp_handle_upload', array( $this, 'handle_post_upload' ) );
    }

    /**
     * Handles pre-upload for when a file is uploaded is in progress.
     *
     * @since 1.0.0
     *
     * @param string[] $file The uploaded file info.
     *
     * @return string[]
     */
    public function handle_pre_upload( $file ) {
        \add_filter( 'upload_dir', array( $this, 'set_upload_directory' ) );

        return $file;
    }

    /**
     * Sets the upload directory during file upload.
     *
     * @since 1.0.0
     *
     * @param array $upload_info The information for uploads (path, url, etc.).
     *
     * @return array
     */
    public function set_upload_directory( $upload_info ) {
        if ( '1' === \get_option( 'directory_uploads' ) ) {
            $filename = $this->form_helper->get_value_from_files( 'name' );

            if ( '' !== $filename ) {
                $directory = strtolower( substr( $filename, 0, strpos( $filename, '.' ) ) );

                $upload_info['subdir'] = $directory;
                $upload_info['path'] .= DIRECTORY_SEPARATOR . $directory;
                $upload_info['url'] .= DIRECTORY_SEPARATOR . $directory;
            }
        }

        return $upload_info;
    }

    /**
     * Handles post-upload for when a file is uploaded is in progress.
     *
     * @since 1.0.0
     *
     * @param array $fileinfo The file information.
     *
     * @return bool|false|int|string
     */
    public function handle_post_upload( $fileinfo ) {
        \remove_filter( 'upload_dir', array( $this, 'set_upload_directory' ) );

        return $fileinfo;
    }
}
