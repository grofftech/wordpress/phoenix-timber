<?php
/**
 * Phoenix Timber Theme
 *
 * @package     GroffTech\PhoenixTimber
 * @since       1.0.0
 * @author      Brett Groff
 * @link        https://grofftech.net
 * @license     GNU General Public License 2.0+
 */

namespace GroffTech\PhoenixTimber;

use GroffTech\PhoenixTimber\Asset\AssetVersioning;
use GroffTech\PhoenixTimber\Customizer\Customizer;
use GroffTech\PhoenixTimber\Log\Log;
use GroffTech\PhoenixTimber\Service\ServiceRegistrar;
use GroffTech\PhoenixTimber\Menus\Menus;
use GroffTech\PhoenixTimber\ThemeSupports\ThemeSupports;
use GroffTech\PhoenixTimber\Timber\TimberHelper;
use GroffTech\PhoenixTimber\File\UploadFile;
use GroffTech\PhoenixTimber\Helpers\PluginHelper;
use GroffTech\PhoenixTimber\Settings\Settings;
use GroffTech\PhoenixTimber\SiteMap\SiteMap;

/**
 * Phoenix Timber class.
 */
class PhoenixTimber extends ServiceRegistrar {
    /**
     * The theme directory.
     *
     * @var string
     */
    private $theme_dir;

    /**
     * The theme url.
     *
     * @var string
     */
    private $theme_url;

    /**
     * The Asset Versioning object.
     *
     * @var AssetVersioning
     */
    private $asset_versioning;

    /**
     * Dependency injector.
     * Overrides the property in the parent class.
     *
     * @var GroffTech\PhoenixTimber\Dependencies\Auryn
     */
    protected $injector;

    /**
     * The list of classes to instantiate.
     *
     * @var array
     */
    protected $classes = array(
        AssetVersioning::class,
        Menus::class,
        ThemeSupports::class,
        TimberHelper::class,
        Log::class,
        UploadFile::class,
        Settings::class,
        Customizer::class,
        SiteMap::class,
    );

    /**
     * The suffix (e.g. .min) for an asset file.
     *
     * @var string
     */
    protected $asset_suffix;

    /**
     * Constructor
     *
     * @param string $theme_dir The theme directory location.
     * @param string $theme_url The theme url.
     * @param GroffTech\PhoenixTimber\Dependencies\Auryn  $injector The dependency injector.
     */
    public function __construct( $theme_dir, $theme_url, $injector ) {
        $this->theme_dir = $theme_dir;
        $this->theme_url = $theme_url;
        $this->injector = $injector;
    }

    /**
     * Bootstrap the theme.

     * Overrides the run method in the parent class.
     *
     * @since 1.0.0
     *
     * @return void
     */
    public function run() {
        $this->init_constants();
        $this->toggle_url_redirection();
        $this->register_hooks();
        parent::run();

        $this->asset_versioning =
            (object) $this->classes['GroffTech\PhoenixTimber\Asset\AssetVersioning'];

        $this->asset_suffix = $this->asset_versioning->development_mode ? '' : '.min';
        $this->set_theme_version_constant();
    }

    /**
     * Initialize the theme constants.
     *
     * @since 1.0.0
     *
     * @return void
     */
    private function init_constants() {
        $child_theme = \wp_get_theme();

        if ( ! defined( 'PHOENIX_TIMBER_DIR' ) ) {
            define( 'PHOENIX_TIMBER_DIR', $this->theme_dir );
        }

        if ( ! defined( 'PHOENIX_TIMBER_URL' ) ) {
            define( 'PHOENIX_TIMBER_URL', $this->theme_url );
        }

        if ( ! defined( 'PHOENIX_TIMBER_NAME' ) ) {
            define( 'PHOENIX_TIMBER_NAME', $child_theme->get( 'Name' ) );
        }
    }

    /**
     * Toggles automatic URL redirection based on debug settings.
     *
     * If debug is off or not defined, redirect will be based on siteUrl settings
     * in the database, otherwise no redirection will happen.
     *
     * @since 1.0.0
     */
    private function toggle_url_redirection() {
        if ( ! defined( 'WP_DEBUG' ) || false === constant( 'WP_DEBUG' ) ) {
            return;
        }

        if ( constant( 'WP_DEBUG' ) ) {
            \remove_filter( 'template_redirect', 'redirect_canonical' );
        }
    }

    /**
     * Registers theme level hooks.
     *
     * @since 1.0.0
     *
     * @return void
     */
    public function register_hooks() {
        \add_action( 'wp_enqueue_scripts', array( $this, 'load_assets' ), 15 );
        \add_action( 'enqueue_block_editor_assets', array( $this, 'load_editor_styles' ), 5 );
        \add_action( 'admin_enqueue_scripts', array( $this, 'load_admin_scripts' ) );
    }

    /**
     * Sets the theme version constant.
     *
     * @since 1.0.0
     *
     * @return void
     */
    private function set_theme_version_constant() {
        if ( ! defined( 'PHOENIX_TIMBER_VERSION' ) ) {
            define(
                'PHOENIX_TIMBER_VERSION',
                $this->asset_versioning->get_theme_version()
            );
        }
    }

    /**
     * Loads styles and scripts for the front end.
     *
     * @since 1.0.0
     */
    public function load_assets() {
        $this->load_styles();
        $this->load_scripts();
        $this->unload_scripts();
    }

    /**
     * Loads styles needed for the front end.
     *
     * @since 1.0.0
     */
    private function load_styles() {
        wp_enqueue_style( 'dashicons' );

        wp_enqueue_style(
            'site-styles',
            $this->asset_versioning->stylesheet_uri,
            array(),
            $this->asset_versioning->get_theme_version(),
            ''
        );
    }

    /**
     * Loads scripts.
     *
     * @since 1.0.0
     */
    public function load_scripts() {
        // Global.
        $asset_file = "/assets/dist/js/theme{$this->asset_suffix}.js";
        \wp_enqueue_script(
            'phoenix-timber',
            PHOENIX_TIMBER_URL . $asset_file,
            array(),
            $this->asset_versioning->get_asset_current_timestamp( PHOENIX_TIMBER_DIR . $asset_file ),
            true
        );
    }

    /**
     * Unload/Dequeue scripts.
     *
     * @since 1.0.0
     *
     * @return void
     */
    public function unload_scripts() {

    }

    /**
     * Loads scripts and styles needed for the back end admin.
     * Does not include scripts for the block editor.
     *
     * @since 1.0.0
     */
    public function load_admin_scripts() {
        // CSS.
        $asset_file = "/assets/dist/css/admin{$this->asset_suffix}.css";
        \wp_enqueue_style(
            'site-admin-style',
            PHOENIX_TIMBER_URL . $asset_file,
            array(),
            $this->asset_versioning->get_asset_current_timestamp( PHOENIX_TIMBER_DIR . $asset_file )
        );
    }

    /**
     * Loads styles for the editor.
     *
     * @since 1.0.0
     *
     * @return void
     */
    public function load_editor_styles() {
        $asset_file = "/assets/dist/css/editor{$this->asset_suffix}.css";
        \wp_enqueue_style(
            'site-editor-styles',
            PHOENIX_TIMBER_URL . $asset_file,
            array(),
            $this->asset_versioning->get_asset_current_timestamp( PHOENIX_TIMBER_DIR . $asset_file ),
            false
        );
    }

    /**
     * Gets a specific class instance.
     *
     * @since 1.0.0
     *
     * @param string $classname The namespace with class name.
     *
     * @return object
     */
    public function get_class( string $classname ) {
        return (object) $this->classes[ $classname ];
    }
}
