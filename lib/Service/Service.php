<?php
/**
 * Service
 *
 * @package     GroffTech\PhoenixTimber\Service
 * @since       1.0.0
 * @author      Brett Groff
 * @link        https://grofftech.net
 * @license     GNU General Public License 2.0+
 */

namespace GroffTech\PhoenixTimber\Service;

use GroffTech\PhoenixTimber\Interfaces\Hookable;
use GroffTech\PhoenixTimber\Interfaces\Runnable;

/**
 * Service abstract class.
 */
abstract class Service implements Runnable, Hookable {
    /**
     * Register hooks for all child classes.
     *
     * @since 1.0.0
     *
     * @return void
     */
    public function run() {
        $this->register_hooks();
    }
}
