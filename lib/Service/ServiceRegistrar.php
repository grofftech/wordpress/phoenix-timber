<?php
/**
 * Registers services required for the theme.
 *
 * @package     GroffTech\PhoenixTimber\Service
 * @since       1.0.0
 * @author      Brett Groff
 * @link        https://grofftech.net
 * @license     GNU General Public License 2.0+
 */

namespace GroffTech\PhoenixTimber\Service;

use GroffTech\PhoenixTimber\Interfaces\Runnable;

/**
 * Service Registrar abstract class.
 */
abstract class ServiceRegistrar implements Runnable {

    /**
     * Classes to instantiate array
     *
     * @var array
     */
    protected $classes = array();

    /**
     * The auryn dependency injector
     *
     * @var mixed
     */
    protected $injector;

    /**
     * Kick off class bootstrap. Should be called by child classes.
     *
     * @since 1.0.0
     *
     * @return void
     */
    public function run() {
        $this->register_services();
    }

    /**
     * Initialize all the services.
     *
     * @since 1.0.0
     *
     * @return void
     */
    protected function register_services() {
        $this->classes = $this->init_classes();

        foreach ( $this->classes as $class ) {
            $class->run();
        }
    }

    /**
     * Initialize a list of classes and return them in an array.
     *
     * @since 1.0.0
     *
     * @return array
     */
    protected function init_classes() {
        $objects = array_map(
            function( $class ) {
					return array(
						'namespace' => $class,
						'object' => $this->injector->make( $class ),
					);
			},
            $this->classes
        );

        return array_column( $objects, 'object', 'namespace' );
    }
}
