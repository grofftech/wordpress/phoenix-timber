<?php
/**
 * Timber Helper
 *
 * @package     GroffTech\PhoenixTimber\Timber
 * @since       1.0.0
 * @author      Brett Groff
 * @link        https://grofftech.net
 * @license     GNU General Public License 2.0+
 */

namespace GroffTech\PhoenixTimber\Timber;

use GroffTech\PhoenixTimber\Helpers\StringHelper;
use GroffTech\PhoenixTimber\Helpers\TaxonomyHelper;
use GroffTech\PhoenixTimber\Service\Service;
use GroffTech\PhoenixTimber\Dependencies\Timber\Twig_Function;
use GroffTech\PhoenixTimber\Dependencies\Timber\Menu;

/**
 * Timber Helper
 */
class TimberHelper extends Service {

    /**
     * Taxonomy Helper object.
     *
     * @var TaxonomyHelper
     */
    private $taxonomy_helper;

    /**
     * String Helper object
     *
     * @var StringHelper
     */
    private $string_helper;

    /**
     * Constructor
     *
     * @param TaxonomyHelper $taxonomy_helper Taxonomy Helper object.
     * @param StringHelper   $string_helper String Helper object.
     */
    public function __construct( TaxonomyHelper $taxonomy_helper, StringHelper $string_helper ) {
        $this->taxonomy_helper = $taxonomy_helper;
        $this->string_helper = $string_helper;
    }

    /**
     * Registers hooks.
     *
     * @since 1.0.0
     *
     * @return void
     */
    public function register_hooks() {
        \add_filter( 'timber/context', array( $this, 'add_to_global_context' ) );
        \add_filter( 'timber/twig', array( $this, 'add_to_twig' ) );
    }

    /**
     * Adds to timber's global context.
     *
     * @since 1.0.0
     *
     * @param array $context The global timber context.
     *
     * @return array $context
     */
    public function add_to_global_context( $context ) {
        $context['menu'] = new Menu( 'primary-menu' );

        $context['theme_uri'] = PHOENIX_TIMBER_URL;

        // $context['svg_sprite'] =
        //     file_get_contents( PHOENIX_TIMBER_DIR . '/assets/dist/svg/sprite.svg' );

        return $context;
    }

    /**
     * Adds functionality to be available in twig templates.
     *
     * @since 1.0.0
     *
     * @param GroffTech\PhoenixTimber\Dependencies\Twig\Environment $twig The twig environment object.
     *
     * @return GroffTech\PhoenixTimber\Dependencies\Twig\Environment
     */
    public function add_to_twig( $twig ) {
        $twig->addFunction( new Twig_Function( 'string_position', array( $this->string_helper, 'get_string_position' ) ) );

        return $twig;
    }
}
