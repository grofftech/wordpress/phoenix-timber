<?php
/**
 * Log Handler
 *
 * @package     GroffTech\PhoenixTimber\Log
 * @since       1.0.0
 * @author      Brett Groff
 * @link        https://grofftech.net
 * @license     GNU General Public License 2.0+
 */

namespace GroffTech\PhoenixTimber\Log;

use Throwable;

/**
 * Log class
 */
class Log {

	/**
     * Constructor
     */
    public function __construct() {
        set_exception_handler( array( $this, 'exception_handler' ) );
    }

    /**
     * Runs any class setup code besides constructor
     *
     * @since 1.0.0
     *
     * @return void
     */
    public function run() {

    }

    /**
     * Global exception handler.
     *
     * Forwards any not caught exceptions to be logged.
     *
     * @since 1.0.0
     *
     * @param \Throwable $ex The exception.
     *
     * @return void
     */
    public function exception_handler( \Throwable $ex ) {
        $this->log_exception( $ex );
    }

    /**
     * Logs a message to the debug log.
     *
     * @since 1.0.0
     *
     * @param string $message The message to log.
     *
     * @return void
     */
    public static function log_message( string $message ) {
        // TODO: if type of array use print_r.

        // phpcs:disable WordPress.PHP.DevelopmentFunctions.error_log_error_log
        error_log( $message );
    }

    /**
     * Logs an exception to the debug log.
     *
     * @since 1.0.0
     *
     * @param \Throwable $ex The exception to log.
     *
     * @return void
     */
    public static function log_exception( \Throwable $ex ) {
        // TODO: Add logging any previous exceptions.

        $message = "\n\n";

        $exception_message = $ex->getMessage();
        $stack_trace = $ex->getTraceAsString();

        if ( '' !== $exception_message ) {
            $message .= 'Message: ';
            $message .= $exception_message . "\n";
        }

        if ( '' !== $stack_trace ) {
            $message .= 'Stack Trace: ' . "\n";
            $message .= $stack_trace;
        }

        $message .= "\n";

        // phpcs:disable WordPress.PHP.DevelopmentFunctions.error_log_error_log
        error_log( $message );
    }
}
