<?php
/**
 * Posts handler
 *
 * @package     GroffTech\PhoenixTimber\Post
 *
 * @since       1.0.0
 * @author      Brett Groff
 * @link        https://grofftech.net
 * @license     GNU General Public License 2.0+
 */

namespace GroffTech\PhoenixTimber\Post;

use GroffTech\PhoenixTimber\Helpers\FormHelper;
use GroffTech\PhoenixTimber\Helpers\UrlHelper;
use GroffTech\PhoenixTimber\Service\Service;
use WP_Query;

/**
 * Posts class.
 */
class Posts extends Service {
    /**
     * Constructor.
     *
     */
    public function __construct() {

    }

    /**
     * Registers hooks.
     *
     * @since 1.0.0
     *
     * @return void
     */
    public function register_hooks() {
        \add_action( 'pre_get_posts', array( $this, 'set_posts_query' ) );

        \add_filter( 'posts_fields', array( $this, 'select_posts' ), 10, 2 );
        \add_filter( 'posts_join', array( $this, 'join_posts' ), 10, 2 );
        \add_filter( 'posts_where', array( $this, 'where_posts' ), 10, 2 );
        \add_filter( 'posts_groupby', array( $this, 'group_by_posts' ), 10, 2 );
        \add_filter( 'post_limits', array( $this, 'limit_posts' ), 10, 2 );
        \add_filter( 'posts_orderby', array( $this, 'order_posts' ), 10, 2 );

        \add_filter( 'the_posts', array( $this, 'filter_retrieved_posts' ), 10, 2 );
    }

    /**
     * Sets the post query before they are retrieved from the database
     * via wp query.
     *
     * @since 1.0.0
     *
     * @param WP_Query $query The wp query object.
     *
     * @return void
     */
    public function set_posts_query( $query ) {

    }

    /**
     * Checks to see if the query is for a non-admin post type archive.
     *
     * @since 1.0.0
     *
     * @param WP_Query $query The wp query object.
     * @param string   $post_type The post type name.
     *
     * @return bool
     */
    private function is_archive_query( $query, $post_type ) {
        // Have to use is_admin property, is_admin() doesn't seem to work properly.
        return ! $query->is_admin && $query->is_post_type_archive( $post_type );
    }

    /**
     * Filters the SELECT portion of a wp query.
     *
     * @since 1.0.0
     *
     * @param string   $fields The fields for the select statement.
     * @param WP_Query $query The wp query object.
     *
     * @return string
     */
    public function select_posts( $fields, $query ) {
        return $fields;
    }

    /**
     * Filters the JOIN portion of a wp query.
     *
     * @since 1.0.0
     *
     * @param string   $join The join for the JOIN statement.
     * @param WP_Query $query The wp query object.
     *
     * @return string
     */
    public function join_posts( $join, $query ) {
        return $join;
    }

    /**
     * Filters the WHERE portion of a wp query.
     *
     * @since 1.0.0
     *
     * @param string   $where The where for the WHERE statement.
     * @param WP_Query $query The wp query object.
     *
     * @return bool|false|int|string
     */
    public function where_posts( $where, $query ) {
        return $where;
    }

    /**
     * Filters the GROUP BY portion of a wp query.
     *
     * @since 1.0.0
     *
     * @param string   $groupby The group by for the GROUP BY statement.
     * @param WP_Query $query The wp query object.
     *
     * @return string
     */
    public function group_by_posts( $groupby, $query ) {
        return $groupby;
    }

    /**
     * Filters the LIMIT portion of a wp query.
     *
     * @since 1.0.0
     *
     * @param array    $limit The limit for the LIMIT statement.
     * @param WP_Query $query The wp query object.
     *
     * @return string
     */
    public function limit_posts( $limit, $query ) {
        return $limit;
    }

    /**
     * Filters the ORDER BY portion of a wp query.
     *
     * @since 1.0.0
     *
     * @param string   $orderby The order by for the ORDER BY statement.
     * @param WP_Query $query The wp query object.
     *
     * @return string
     */
    public function order_posts( $orderby, $query ) {
        return $orderby;
    }

    /**
     * Filters posts after they have been retrieved from the database.
     *
     * @since 1.0.0
     *
     * @param array    $posts The retrieved posts.
     * @param WP_Query $query The wp query object.
     *
     * @return array
     */
    public function filter_retrieved_posts( $posts, $query ) {
        return $posts;
    }
}
