<?php
/**
 * Site Map
 *
 * @package     GroffTech\PhoenixTimber\SiteMap
 * @since       1.0.0
 * @author      Brett Groff
 * @link        https://grofftech.net
 * @license     GNU General Public License 2.0+
 */

namespace GroffTech\PhoenixTimber\SiteMap;

use GroffTech\PhoenixTimber\Helpers\PluginHelper;
use GroffTech\PhoenixTimber\Service\Service;

/**
 * Site map class
 */
class SiteMap extends Service {

    /**
     * The plugin helper object.
     *
     * @var PluginHelper
     */
    private $plugin_helper;

    /**
     * Constructor.
	 *
	 * @param PluginHelper $plugin_helper The plugin helper object.
     */
    public function __construct( PluginHelper $plugin_helper ) {
        $this->plugin_helper = $plugin_helper;
    }

    /**
     * Registers hooks.
     *
     * @since 1.0.0
     *
     * @return void
     */
    public function register_hooks() {
		\add_filter( 'wp_sitemaps_enabled', '__return_false' );
        \add_action( 'publish_post', array( $this, 'create_sitemap' ) );
        \add_action( 'publish_page', array( $this, 'create_sitemap' ) );
    }

    /**
     * Creates the sitemap.
     *
     * @since 1.0.0
     *
     * @return void
     */
    public function create_sitemap() {
        $post_types = \apply_filters(
            'sitemap_post_types',
            array( 'post', 'page' )
        );

        $post_slugs_to_exclude = \apply_filters(
            'sitemap_post_exclude_slugs',
            array()
        );
        $post_ids_to_exclude = array();

        $woocommerce_active = $this->plugin_helper->is_plugin_active( 'woocommerce/woocommerce.php' );

        if ( $woocommerce_active ) {
            array_push( $post_types, 'product' );
            array_push( $post_slugs_to_exclude, 'cart', 'checkout', 'my-account' );
        }

        if ( count( $post_slugs_to_exclude ) > 0 ) {
            foreach ( $post_slugs_to_exclude as $post_slug ) {
                $post = \get_page_by_path( $post_slug );
                array_push( $post_ids_to_exclude, $post->ID );
            }
        }

        $posts_for_sitemap = \get_posts(
            array(
				'numberposts' => -1,
				'orderby' => 'modified',
				'post_type' => $post_types,
                'exclude' => $post_ids_to_exclude,
				'post_status' => 'publish',
				'order' => 'DESC',
            )
        );

        $sitemap_xml = $this->create_sitemap_xml( $posts_for_sitemap );

        $fp = fopen( ABSPATH . '/sitemap.xml', 'w' ); // phpcs:ignore
        fwrite( $fp, $sitemap_xml ); // phpcs:ignore
        fclose( $fp ); // phpcs:ignore
    }

    /**
     * Creates the XML for the sitemap.
     *
     * @since 1.0.0
     *
     * @param array $posts The list of posts.
     *
     * @return string
     */
    private function create_sitemap_xml( array $posts ) {
        $xml = '<?xml version="1.0" encoding="UTF-8"?>';
        $xml .= '<?xml-stylesheet type="text/xsl" href="sitemap-style.xsl"?>';
        $xml .= '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';

        foreach ( $posts as $post ) {
            \setup_postdata( $post );

            $post_date = explode( ' ', $post->post_modified );

            $xml .= '<url>'
                . '<loc>' . \get_permalink( $post->ID ) . '</loc>'
                . '<priority>1</priority>'
                . '<lastmod>' . $post_date[0] . '</lastmod>'
                . '<changefreq>daily</changefreq>'
                . '</url>';
        }

        $xml .= '</urlset>';

        return $xml;
    }
}
