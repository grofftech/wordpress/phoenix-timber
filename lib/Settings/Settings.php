<?php
/**
 * Theme settings handler.
 *
 * @package     GroffTech\PhoenixTimber\Settings
 * @since       1.0.0
 * @author      Brett Groff
 * @link        https://grofftech.net
 * @license     GNU General Public License 2.0+
 */

namespace GroffTech\PhoenixTimber\Settings;

use GroffTech\PhoenixTimber\Service\Service;
use GroffTech\PhoenixTimber\Dependencies\Timber\Timber;

/**
 * Settings class.
 */
class Settings extends Service {

    /**
     * The Timber object.
     *
     * @var Timber
     */
    private $timber;

    /**
     * Constructor.
     *
     * @param GroffTech\PhoenixTimber\Dependencies\Timber $timber The Timber object.
     */
    public function __construct( Timber $timber ) {
        $this->timber = $timber;
    }

    /**
     * Registers hooks.
     *
     * @since 1.0.0
     *
     * @return void
     */
    public function register_hooks() {
        \add_action( 'admin_init', array( $this, 'initialize_theme_settings' ) );
        \add_action( 'admin_menu', array( $this, 'create_theme_settings_menu' ) );
    }

    /**
     * Initialize theme settings.
     *
     * @since 1.0.0
     *
     * @return void
     */
    public function initialize_theme_settings() {
        // TODO: Make this configurable.
        \add_settings_section(
            'media_options_section', // id.
            'Media Options', // title.
            array( $this, 'render_media_options' ), // callback.
            'media_options' // page.
        );

        \add_settings_field(
            'directory_uploads', // id.
            '', // title.
            array( $this, 'render_directory_uploads' ), // callback.
            'media_options', // page.
            'media_options_section', // section.
            array( // args.
                'class' => 'directory-uploads',
            )
        );

        \register_setting(
            'media_options',
            'directory_uploads'
        );
    }

    /**
     * Renders the markup for the media options section.
     *
     * @since 1.0.0
     *
     * @return void
     */
    public function render_media_options() {
        $data = array();
        $data['help_message'] = 'Options for handling media.';

        $this->timber->render( 'views/admin/theme-settings/media-options-help.twig', $data );
    }

    /**
     * Renders the markup for the directory uploads setting.
     *
     * @since 1.0.0
     *
     * @return void
     */
    public function render_directory_uploads() {
        $data = array();
        $data['checked'] = \checked( 1, \get_option( 'directory_uploads' ), false );
        $data['label'] = 'Create directory for file upload based on the filename';

        $this->timber->render(
            'views/admin/theme-settings/directory-uploads.twig',
            $data
        );
    }

    /**
     * Creates the theme settings menu.
     *
     * @since 1.0.0
     *
     * @return void
     */
    public function create_theme_settings_menu() {
        \add_theme_page(
            'Phoenix Timber Settings',
            'Phoenix Timber',
            'administrator',
            'phoenix_timber_settings',
            array( $this, 'render_theme_settings' )
        );
    }

    /**
     * Renders the markup for the theme settings page.
     *
     * @since 1.0.0
     *
     * @return void
     */
    public function render_theme_settings() {
        $data = array();
        $this->timber->render( 'views/admin/theme-settings/theme-settings.twig', $data );
    }
}
