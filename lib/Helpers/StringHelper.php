<?php
/**
 * String Helpers
 *
 * @package     GroffTech\PhoenixTimber\Helpers
 * @since       1.0.0
 * @author      Brett Groff
 * @link        https://grofftech.net
 * @license     GNU General Public License 2.0+
 */

namespace GroffTech\PhoenixTimber\Helpers;

/**
 * String Helper class.
 */
class StringHelper {

    /**
     * Constructor
     */
    public function __construct() {

    }

    /**
     * Gets the position of the first occurrence of a string in a string
     *
     * @since 1.0.0
     *
     * @param string $string_to_search The string to perform the search on.
     * @param string $string_to_find   The string to find in the search string.
     * @param string $occurrence       The occurrence to find (first, last).
     * @param int    $start_offset     The position offset to start the search. If
     *                                 negative, starts the search from the end.
     * @param string $encoding         The encoding to use, default is UTF-8.
     *
     * @return bool|false|int|string
     */
    public function get_string_position( string $string_to_search, string $string_to_find, $occurrence = 'first', $start_offset = 0, $encoding = 'UTF-8' ) {
        if ( 'first' === $occurrence ) {
            return mb_strpos(
                $string_to_search,
                $string_to_find,
                $start_offset,
                $encoding
            );
        }

        if ( 'last' === $occurrence ) {
            return mb_strrpos(
                $string_to_search,
                $string_to_find,
                $start_offset,
                $encoding
            );
        }
    }

    /**
     * Checks to see if a string starts with a specific string.
     *
     * @since 1.0.0
     * @param string $string_to_search  The string to search.
     * @param string $string_to_find    The string to search for.
     * @param string $encoding          The encoding to use, default is UTF-8.
     *
     * @return bool
     */
    public function string_starts_with( $string_to_search, $string_to_find, $encoding = 'UTF-8' ) {
        $needle_length = mb_strlen( $string_to_find, $encoding );

        return mb_substr( $string_to_search, 0, $needle_length, $encoding ) === $string_to_find;
    }

    /**
     * Checks to see if a string ends with a specific string.
     *
     * @since 1.0.0
     *
     * @param string $string_to_search  The string to search.
     * @param string $string_to_find    The string to search for.
     * @param string $encoding          The encoding to use, default is UTF-8.
     *
     * @return bool
     */
    public function string_ends_with( $string_to_search, $string_to_find, $encoding = 'UTF-8' ) {
        $starting_offset = -mb_strlen( $string_to_find, $encoding );

        return mb_substr( $string_to_search, $starting_offset, null, $encoding ) === $string_to_find;
    }

    /**
     * Checks to see if a substring exists in a string.
     *
     * @since 1.0.0
     *
     * @param string $string_to_search  The string to search.
     * @param string $string_to_find    The string to search for.
     * @param string $encoding          The encoding to use, default is UTF-8.
     *
     * @return int The numeric position of the string searching for.
     */
    public function string_has_substring( $string_to_search, $string_to_find, $encoding = 'UTF-8' ) {
        return mb_strpos( $string_to_search, $string_to_find, 0, $encoding ) !== false;
    }

    /**
     * Truncates (cuts off) a string by a set number of characters
     *
     * @since 1.0.0
     *
     * @param string $string_to_truncate The string to truncate.
     * @param int    $character_limit    The number of characters to keep in the string,
     *                                   default is 100.
     * @param string $ending_suffix      The string that goes at the end after the
     *                                   characters are truncated, default is '...'.
     * @param string $encoding           The encoding to use, default is UTF-8.
     *
     * @return string Returns the truncated string with the ending suffix.
     */
    public function truncate_by_number_of_characters( $string_to_truncate, $character_limit = 100, $ending_suffix = '...', $encoding = 'UTF-8' ) {
        $character_count = mb_strwidth( $string_to_truncate, $encoding );

        if ( $character_count <= $character_limit ) {
            return $string_to_truncate;
        }

        $string_to_truncate = \wp_strip_all_tags( $string_to_truncate );
        $truncated_string = mb_strimwidth( $string_to_truncate, 0, $character_limit, '', $encoding );

        return rtrim( $truncated_string ) . $ending_suffix;
    }

    /**
     * Truncates (cuts off) a string by a set number of words.
     *
     * @since 1.0.0
     *
     * @param string $string_to_truncate    The string to truncate.
     * @param int    $word_limit            The number of words to keep in the string, default is 100.
     * @param string $ending_suffix         The string that goes at the end after the words are truncated,
     *                                      default is '...'.
     *
     * @return string The truncated string with the ending suffix.
     */
    public function truncate_by_number_of_words( $string_to_truncate, $word_limit = 100, $ending_suffix = '...' ) {
        $string_to_truncate = \wp_strip_all_tags( $string_to_truncate );
        preg_match( '/^\s*+(?:\S++\s*+){1,' . $word_limit . '}/u', $string_to_truncate, $matches );

        if ( ! isset( $matches[0] ) ) {
            return $string_to_truncate;
        }

        if ( mb_strlen( $string_to_truncate ) === mb_strlen( $matches[0] ) ) {
            return $string_to_truncate;
        }

        return rtrim( $matches[0] ) . $ending_suffix;
    }

    /**
     * Converts a string to lowercase
     *
     * @since 1.0.0
     * @param string $string_to_convert The string to convert.
     * @param string $encoding The encoding to use, default is UTF-8.
     *
     * @return string Returns the string in lowercase.
     */
    public function convert_string_to_lowercase( $string_to_convert, $encoding = 'UTF-8' ) {
        return mb_strtolower( $string_to_convert, $encoding );
    }

    /**
     * Converts a string to uppercase
     *
     * @since 1.0.0
     * @param string $string_to_convert The string to convert.
     * @param string $encoding The encoding to use, default is UTF-8.
     *
     * @return string Returns the string in uppercase.
     */
    public function convert_string_to_uppercase( $string_to_convert, $encoding = 'UTF-8' ) {
        return mb_strtoupper( $string_to_convert, $encoding );
    }

    /**
     * Converts a string into an array.
     *
     * @since 1.0.0
     *
     * @param string $string_to_convert The string to convert into an array.
     * @param string $separator         The separator(comma, space, etc.) to use for the conversion.
     *
     * @return array The string as an array.
     */
    public function convert_string_to_array( $string_to_convert, $separator ) {
        if ( ! $this->string_has_substring( $string_to_convert, $separator ) ) {
            return;
        }

        $converted_array = explode( $separator, $string_to_convert );

        return array_map( 'trim', $converted_array );
    }
}
