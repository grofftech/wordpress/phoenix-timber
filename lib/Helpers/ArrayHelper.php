<?php
/**
 * Array Helper.
 *
 * @package     GroffTech\PhoenixTimber\Helpers
 * @since       1.0.0
 * @author      Brett Groff
 * @link        https://grofftech.net
 * @license     GNU General Public License 2.0+
 */

namespace GroffTech\PhoenixTimber\Helpers;

/**
 * Array Helpers class.
 */
class ArrayHelper {

    /**
     * Constructor
     */
    public function __construct() {

    }

    /**
     * A case insensitive check to see if an array's keys has a keyword in it.
     * Once the keyword is found, it will stop checking.
     *
     * @since 1.0.0
     *
     * @param array  $arr     The associative array to check.
     * @param string $keyword The word to check for.
     *
     * @return bool|false|int|string
     */
    public function array_keys_has_keyword( array $arr, string $keyword ) {
        // TODO: Write tests for me.
        $result = false;

        $keys = array_keys( $arr );

        for ( $i = 0; $i < count( $keys ); $i++ ) { //phpcs:ignore
            $key = strtolower( $keys[ $i ] );
            if ( strpos( $key, $keyword ) !== false ) {
                $result = true;
                break;
            }
        }

        return $result;
    }
}
