<?php
/**
 * Form Helper
 *
 * @package     GroffTech\PhoenixTimber\Helpers
 * @since       1.0.0
 * @author      Brett Groff
 * @link        https://grofftech.net
 * @license     GNU General Public License 2.0+
 */

namespace GroffTech\PhoenixTimber\Helpers;

use Exception;

/**
 * Form Helper
 */
class FormHelper {

	/**
     * Constructor
     */
    public function __construct() {

    }

    /**
     * Gets text field data from global $_POST and sanitizes it.
     *
     * @since 1.0.0
     *
     * @param string $request_type  The request type as a string (get, post).
     * @param string $value         The name attribute from the text field input.
     * @param string $nonce_action  The nonce action name. Default is empty string.
     * @param string $nonce_key     The nonce key. Default is empty string.
     *
     * @return string
     */
    public function get_text_field( string $request_type, string $value, string $nonce_action = '', $nonce_key = '' ) {
        $request = $this->get_request( $request_type );
        $result = '';

        $is_nonce_set = '' !== $nonce_action && $this->is_value_set( $request_type, $nonce_key );
        $is_value_set = $this->is_value_set( $request_type, $value );

        // Check nonce and get value.
        if ( $is_nonce_set ) {
            $nonce = \sanitize_text_field( \wp_unslash( $request[ $nonce_key ] ) ); // phpcs:ignore

            if ( $is_value_set && $this->is_nonce_valid( $nonce_action, $nonce ) ) {
                $result = \sanitize_text_field( \wp_unslash( $request[ $value ] ) ); // phpcs:ignore
            } else {
                return false;
            }
        }

        // Check value and get it.
        if ( ! $is_nonce_set && $is_value_set ) {
            $result = \sanitize_text_field( \wp_unslash( $request[ $value ] ) ); // phpcs:ignore
        }

        return $result;
    }

    /**
     * Gets the associated superglobal request array based on
     * the string type.
     *
     * @since 1.0.0
     *
     * @param string $request_type The request type as a string (get, post).
     *
     * @throws \Exception When the request type is invalid.
     *
     * @return array
     */
    private function get_request( $request_type ) {
        $type = strtolower( $request_type );
        $request = array();

        if ( 'post' === $type ) {
            $request = $_POST;
        }

        if ( 'get' === $type ) {
            $request = $_GET; // phpcs:ignore
        }

        return $request;
    }

    /**
     * Gets checkbox data from global $_POST variable as a yes or no
     * string.
     *
     * @since 1.0.0
     *
     * @param string $request_type  The request type as a string (get, post).
     * @param string $name          The name attribute from the checkbox input.
     *
     * @return string yes if the name is set, otherwise no
     */
    public function get_checkbox( string $request_type, string $name ) {
        // TODO: Add optional nonce checking.
        $request = $this->get_request( $request_type );
        $result = 'no';

        if ( isset( $request[ $name ] ) ) {
            $result = 'yes';
        }

        return $result;
    }

    /**
     * Gets integer value from global _POST.
     *
     * @since 1.0.0
     *
     * @param string $request_type  The request type as a string (post, get).
     * @param string $value         The value.
     *
     * @return int
     */
    public function get_integer( string $request_type, string $value ) {
        // TODO: Add optional nonce checking.
        $request = $this->get_request( $request_type );
        $result = 0;

        if ( isset( $request[ $value ] ) ) {
            $result = intval( $request[ $value ] );
        }

        return $result;
    }

    /**
     * Checks to see if a value is set
     *
     * @since 1.0.0
     *
     * @param string $request_type  The request type as a string (post, get, etc.).
     * @param string $value         The value in the array to check.
     *
     * @return bool
     */
    public function is_value_set( string $request_type, string $value ) {
        $request = $this->get_request( $request_type );

        return isset( $request[ $value ] );
    }

    /**
     * Checks to see if a nonce is valid.
     *
     * @since 1.0.0
     *
     * @param string $nonce_action  The nonce action name.
     * @param string $nonce         The nonce value.
     *
     * @return int|false
     */
    public function is_nonce_valid( string $nonce_action, string $nonce ) {
        return \wp_verify_nonce( $nonce, $nonce_action );
    }

    /**
     * Decode json value from global $_POST array.
     *
     * @since 1.0.0
     *
     * @param string  $value             The value from the $_POST array.
     * @param string  $nonce_action      The nonce action name. Default is empty string.
     * @param string  $nonce_key         The nonce key. Default is empty string.
     * @param bool    $associative_array A flag to indicate if json should
     *                                   be returned in an associative array.
     * @param integer $depth             The recursion depth.
     * @param integer $flags             Bit mask of json decode options.
     *
     * @link https://php.net/manual/en/function.json-decode.php
     * @return object|array Returns an object when associative array is false and an array when true.
     */
    public function get_json_from_post( $value, $nonce_action = '', $nonce_key = '', $associative_array = false, $depth = 512, $flags = 0 ) {
        if ( ! $this->is_value_set( 'post', $value ) ) {
            return null;
        }

        $json = $this->get_text_field( 'post', $value, $nonce_action, $nonce_key );

        if ( false === $json ) {
            $this->display_invalid_message();
        }

        $result = json_decode( $json, $associative_array, $depth, $flags );

        if ( json_last_error() !== 0 ) {
            return null;
        }

        return $result;
    }

    /**
     * Gets a value by key from the global $_FILES array.
     *
     * @since 1.0.0
     *
     * @param string $key The key from the $_FILES array.
     *
     * @return string
     */
    public function get_value_from_files( $key ) {
        $result = '';

        if ( isset( $_FILES['async-upload'][ $key ] ) && 'name' === $key ) {
            $result = \sanitize_file_name( \wp_unslash( $_FILES['async-upload'][ $key ] ) );
        }

        return $result;
    }

    /**
     * Description
     *
     * @since 1.0.0
     *
     * @return void
     */
    public function display_invalid_message() {
        // TODO. Figure out what to do here.
        die( \esc_html__( 'The request is invalid, please try again.', 'phoenix-timber' ) );
    }
}
