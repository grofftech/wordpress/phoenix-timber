<?php
/**
 * Taxonomy Helper
 *
 * @package     GroffTech\PhoenixTimber\Helpers
 * @since       1.0.0
 * @author      Brett Groff
 * @link        https://grofftech.net
 * @license     GNU General Public License 2.0+
 */

namespace GroffTech\PhoenixTimber\Helpers;

use Exception;

/**
 * Taxonomy Class.
 */
class TaxonomyHelper {

	/**
     * Constructor.
     */
    public function __construct() {

    }

    /**
     * Get Taxonomy Terms
     *
     * When only taxonomy name is supplied will use defaults which includes
     * ordering by name.
     *
     * When no taxonomy name is supplied and options
     * are passed in, will use options to get terms.
     *
     * @param string $taxonomy_name The taxonomy name.
     * @param array  $options Options array.
     *
     * @since 1.0.0
     *
     * @throws \Exception When options is missing taxonomy name.
     *
     * @return WP_Term[]|int|WP_Error List of WP_Term instances and their children. Will return WP_Error, if any of taxonomies do not exist.
     */
    public function get_taxonomy_terms( $taxonomy_name = '', $options = array() ) {
        $defaults = array(
            'taxonomy' => $taxonomy_name,
            'hide_empty' => false,
            'order_by' => 'name',
        );

        if ( '' !== $taxonomy_name && empty( $options ) ) {
            return \get_terms( $defaults );
        }

        if ( empty( $options['taxonomy'] ) ) {
            throw new \Exception( "'taxonomy' key and value required in the options array" );
        }

        return \get_terms( $options );
    }

    /**
     * Get the name property from an array of taxonomy terms.
     *
     * @since 1.0.0
     *
     * @param \WP_Term[] $terms The array of taxonomy terms.
     *
     * @return array
     */
    public function get_taxonomy_term_names( $terms ) {
        $term_names = array();

        foreach ( $terms as $term ) {
            $name = $term->name;
            array_push( $term_names, $name );
        }

        return $term_names;
    }
}
