<?php
/**
 * Plugin Helper
 *
 * @package     GroffTech\PhoenixTimber\Helpers
 * @since       1.0.0
 * @author      Brett Groff
 * @link        https://grofftech.net
 * @license     GNU General Public License 2.0+
 */

namespace GroffTech\PhoenixTimber\Helpers;

/**
 * Plugin helper class.
 */
class PluginHelper {

    /**
     * Constructor
     */
    public function __construct() {
    }

    /**
     * Checks to see if a plugin is active
     *
     * @since 1.0.0
     *
     * @param string $plugin_base_path The base path of the plugin file (e.g.
	 *                                 woocommerce/woocommerce.php).
     *
     * @return bool
     */
    public function is_plugin_active( string $plugin_base_path ) {
        $result = false;

        if ( in_array( $plugin_base_path, apply_filters( 'active_plugins', get_option( 'active_plugins ' ) ), true ) ) {
			$result = true;
		}

        return $result;
    }
}
