<?php
/**
 * Url Helper
 *
 * @package     GroffTech\PhoenixTimber\Helpers
 * @since       1.0.0
 * @author      Brett Groff
 * @link        https://grofftech.net
 * @license     GNU General Public License 2.0+
 */

namespace GroffTech\PhoenixTimber\Helpers;

use GroffTech\PhoenixTimber\Helpers\StringHelper;
use GroffTech\PhoenixTimber\Service\Service;

/**
 * Url Helper class.
 */
class UrlHelper extends Service {
    /**
     * The string helper object.
     *
     * @var StringHelper
     */
    private $string_helper;

    /**
     * Constructor
     *
     * @param StringHelper $string_helper The string helper object.
     */
    public function __construct( StringHelper $string_helper ) {
        $this->string_helper = $string_helper;
    }

    /**
     * Register hooks.
     *
     * @since 1.0.0
     *
     * @return void
     */
    public function register_hooks() {

    }

    /**
     * Gets the page number for the url.
     *
     * For example on an archive page, if the url is like /shop/page/2/
     * this should return 2.
     *
     * @since 1.0.0
     *
     * @param string $url The url to check. This can be either the full url with
     *                    http or the uri.
     *
     * @return string
     */
    public function get_page_from_url( $url ) {
        // First check for the page after slash (e.g. page/1/).
        $page_number = mb_substr(
            $url,
            $this->string_helper->get_string_position( $url, '/', 'last' ) - 1,
            1
        );

        // When there's no slash at the end (e.g. page/1).
        if ( intval( $page_number ) === 0 ) {
            $page_number = mb_substr(
                $url,
                $this->string_helper->get_string_position( $url, '/', 'last' ) + 1,
                1
            );
        }

        return $page_number;
    }

    /**
     * Parses the url into separate pieces.
     *
     * @since 1.0.0
     *
     * @param string $url       The url to parse.
     * @param int    $component A specific component to return. The default is -1.
     *
     * @return array|false An array if the parse is successful, otherwise false.
     */
    public function parse_url( $url, $component = -1 ) {
        return \wp_parse_url( $url, $component );
    }

    /**
     * Gets the query string part of a url as an associative array.
     *
     * For example, a query string part of key=value will return key => value
     * in the array.
     *
     * @since 1.0.0
     *
     * @param string $query_string The query string to parse (e.g. key=value ).
     *
     * @return bool|false|int|string
     */
    public function get_query_string_as_array( $query_string ) {
        $results = array();

        if ( ! $this->string_helper->get_string_position( $query_string, '&' ) ) {
            $key = substr(
                $query_string,
                0,
                $this->string_helper->get_string_position( $query_string, '=' )
            );

            $value = substr(
                $query_string,
                $this->string_helper->get_string_position( $query_string, '=' ) + 1
            );

            $results[ $key ] = $value;
        } else {
            $queries = explode( '&', $query_string );
            foreach ( $queries as $query ) {
                $key = substr(
                    $query,
                    0,
                    $this->string_helper->get_string_position( $query, '=' )
                );

                $value = substr(
                    $query,
                    $this->string_helper->get_string_position( $query, '=' ) + 1
                );

                $results[ $key ] = $value;
            }
        }

        return $results;
    }
}
