<?php
/**
 * Asset Versioning
 *
 * @package     GroffTech\PhoenixTimber\Asset
 * @since       1.0.0
 * @author      Brett Groff
 * @link        https://grofftech.net
 * @license     GNU General Public License 2.0+
 */

namespace GroffTech\PhoenixTimber\Asset;

use GroffTech\PhoenixTimber\Service\Service;

/**
 * Asset Versioning class.
 */
class AssetVersioning extends Service {

    /**
     * Development mode
     *
     * @var bool
     */
    public $development_mode;

    /**
     * Stylesheet Url
     *
     * @var string
     */
    public $stylesheet_uri;

    /**
     * Constructor.
     */
    public function __construct() {

    }

    /**
     * Overrides the run method of the Service abstract class.
     *
     * @since 1.0.0
     *
     * @return void
     */
    public function run() {
        $this->development_mode = $this->is_site_in_development_mode();
        $this->stylesheet_uri = $this->toggle_stylesheet();
    }

    /**
     * Registers hooks.
     *
     * @since 1.0.0
     *
     * @return void
     */
    public function register_hooks() {

    }

    /**
     * Gets the theme version.
     *
     * In development mode gets the current timestamp of the style.css file.
     * In non-development mode gets the version number at the top of the comment
     * block of the style.css file.
     *
     * @since 1.0.0
     *
     * @return int|string The current timestamp\theme version.
     */
    public function get_theme_version() {
        $development_mode = $this->development_mode;

        if ( $development_mode ) {
            return $this->get_asset_current_timestamp( get_stylesheet_directory() . '/style.css' );
        }

        $theme = wp_get_theme();

        return $theme->get( 'Version' );
    }

    /**
     * Get the current timestamp of an asset file.
     *
     * @since 1.0.0
     *
     * @param string $asset_file The directory location of the asset file.
     *
     * @return int The timestamp
     */
    public function get_asset_current_timestamp( $asset_file ) {
        return filemtime( $asset_file );
    }

    /**
     * Checks to see if the site is in development mode.
     *
     * @since 1.0.0
     *
     * @return bool True if script debug config setting is set, otherwise false
     */
    private function is_site_in_development_mode() {
        if ( ! defined( 'SCRIPT_DEBUG' ) ) {
            return false;
        }

        return constant( 'SCRIPT_DEBUG' );
    }

    /**
     * Loads the non-minified or minified stylesheet.
     *
     * @since 1.0.0
     *
     * @return string The uri of the stylesheet
     */
    private function toggle_stylesheet() {
        if ( $this->development_mode ) {
            return get_stylesheet_uri();
        }

        return get_stylesheet_directory_uri() . '/style.min.css';
    }
}
