<div class="notice notice-error is-dismissible">
    <p><?php echo esc_html( $message ); ?></p>
</div>