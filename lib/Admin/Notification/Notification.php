<?php
/**
 * Notification Handler
 *
 * @package     GroffTech\PhoenixTimber\Admin\Notification
 * @since       1.0.0
 * @author      Brett Groff
 * @link        https://grofftech.net
 * @license     GNU General Public License 2.0+
 */

namespace GroffTech\PhoenixTimber\Admin\Notification;

/**
 * Notification class.
 */
class Notification {

    /**
     * Constructor.
     */
    public function __construct() {
    }

    /**
     * Show an error message.
     *
     * @since 1.0.0
     *
     * @param string $message The message.
     *
     * @return void
     */
    public function show_error_message( $message ) {
        // Add a custom action so we can use the message parameter.
        add_action( 'phoenix_timber_error_message', array( $this, 'render_error_message' ), 10, 1 );

        // Do the custom action with the message.
		add_action(
            'admin_notices',
            function() use ( $message ) {
				do_action( 'phoenix_timber_error_message', $message );
			}
        );
    }

    /**
     * Render the error message.
     *
     * @since 1.0.0
     *
     * @param string $message The message.
     *
     * @return void
     */
    public function render_error_message( $message ) {
        include __DIR__ . '/Views/Error.php';
    }
}
