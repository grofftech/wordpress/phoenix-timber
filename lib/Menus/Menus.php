<?php
/**
 * Creates multiple menus.
 *
 * @package     GroffTech\PhoenixTimber\Setup
 * @since       1.0.0
 * @author      Brett Groff
 * @link        https://grofftech.net
 * @license     GNU General Public License 2.0+
 */

namespace GroffTech\PhoenixTimber\Menus;

use GroffTech\PhoenixTimber\Service\Service;

/**
 * Menus
 */
class Menus extends Service {

    /**
     * Constructor.
     */
    public function __construct() {

    }

    /**
     * Registers hooks.
     *
     * @since 1.0.0
     *
     * @return void
     */
    public function register_hooks() {
        \add_action( 'init', array( $this, 'register_menus' ) );
    }

    /**
     * Registers an array of menus.
     *
     * @since 1.0.0
     *
     * @return void
     */
    public function register_menus() {
        $menus = array(
            'header_menu' => __( 'Header' ),
            'footer_menu' => __( 'Footer' ),
        );

        \register_nav_menus( $menus );
    }

    /**
     * Registers menu by id and name.
     *
     * @since 1.0.0
     *
     * @param int $menu_id The menu id.
     * @param int $menu_name The menu name.
     *
     * @return void
     */
    public function register_menu( $menu_id, $menu_name ) {
        \register_nav_menu( $menu_id, $menu_name );
    }
}
