<?php
/**
 * Customizer handler
 *
 * @package     GroffTech\PhoenixTimber\Customizer
 * @since       1.0.0
 * @author      Brett Groff
 * @link        https://grofftech.net
 * @license     GNU General Public License 2.0+
 */

namespace GroffTech\PhoenixTimber\Customizer;

use GroffTech\PhoenixTimber\Service\Service;
use WP_Customize_Manager;

/**
 * Customizer class
 */
class Customizer extends Service {
    /**
     * Constructor
     */
    public function __construct() {

    }

    /**
     * Registers hooks.
     *
     * @since 1.0.0
     *
     * @return void
     */
    public function register_hooks() {
        \add_action( 'customize_register', array( $this, 'register_customizer' ) );
    }

    /**
     * Registers customizer settings.
     *
     * @since 1.0.0
     *
     * @param WP_Customize_Manager $wp_customize The customize manager object.
     *
     * @return void
     */
    public function register_customizer( $wp_customize ) {

    }

    /**
     * Hides unnecessary sections/panels in the customizer.
     *
     * @param object $wp_customize The wp customizer object.
     */
    public function hide_customizer_sections( $wp_customize ) {

    }
}
