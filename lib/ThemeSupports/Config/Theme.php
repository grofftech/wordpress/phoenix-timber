<?php
/**
 * Theme Supports Configuration
 *
 * @package     GroffTech\PhoenixTimber\ThemeSupports\Config
 * @since       1.0.0
 * @author      Brett Groff
 * @link        https://grofftech.net
 * @license     GNU General Public License 2.0+
 */

namespace GroffTech\PhoenixTimber\ThemeSupports\Config;

/**
 * An array of features for theme support.
 *
 * For each key in the features array, provide the applicable feature name
 * for the name key and optional arguments for the args key where present.
 * The args key where applicable must be first.
 *
 * Those with empty name values will not be enabled.
 *
 * @see https://developer.wordpress.org/reference/functions/add_theme_support/.
 * @see https://developer.wordpress.org/block-editor/developers/themes/theme-support/.
 */
return array(
    'features' => array(
        'custom_logo' => array(
            'args' => array(),
            'name' => '',
        ),
        'feed_links' => array(
            'name' => '',
        ),
        'post_formats' => array(
            'args' => array(),
            'name' => '',
        ),
        'post_thumbnails' => array(
            'args' => array(),
            'name' => '',
        ),
        'custom_background' => array(
            'args' => array(),
            'name' => '',
        ),
        'custom_header' => array(
            'args' => array(),
            'name' => '',
        ),
        'html5' => array(
            'args' => array(),
            'name' => '',
        ),
        'title_tag' => array(
            'name' => '',
        ),
        'custom_selective_refresh_widgets' => array(
            'name' => '',
        ),
        // Block Editor Supports.
        'align_wide' => array(
            'name' => 'align-wide',
        ),
        'editor_color_palette' => array(
            'args' => array(
                array(
                    'name' => __( 'white', 'phoenix-timber' ),
                    'slug' => 'white',
                    'color' => '#ffffff',
                ),
                array(
                    'name' => __( 'black', 'phoenix-timber' ),
                    'slug' => 'black',
                    'color' => '#000000',
                ),
            ),
            'name' => '',
        ),
        'editor_font_sizes' => array(
            'args' => array(
                array(
                    'name' => 'Normal',
                    'size' => 16,
                    'slug' => 'normal',
                ),
                array(
                    'name' => 'Large',
                    'size' => 24,
                    'slug' => 'large',
                ),
            ),
            'name' => '',
        ),
        'disable_custom_font_sizes' => array(
            'name' => '',
        ),
        'disable_custom_colors' => array(
            'name' => '',
        ),
        'disable_custom_gradients' => array(
            'name' => '',
        ),
        'wp_block_styles' => array(
            'name' => '',
        ),
        'editor_gradient_presets' => array(
            'args' => array(),
            'name' => '',
        ),
        'editor_styles' => array(
            'name' => '',
        ),
        'dark_editor_style' => array(
            'name' => '', // editor styles is required for this to work.
        ),
        'woo_commerce' => array(
            'args' => array(),
            'name' => '',
        ),
        'wc_product_gallery_lightbox' => array(
            'name' => '',
        ),
    ),
);
