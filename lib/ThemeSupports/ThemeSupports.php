<?php
/**
 * Theme Supports Handler
 *
 * @package     GroffTech\PhoenixTimber\Setup
 * @since       1.0.0
 * @author      Brett Groff
 * @link        https://grofftech.net
 * @license     GNU General Public License 2.0+
 */

namespace GroffTech\PhoenixTimber\ThemeSupports;

use GroffTech\PhoenixTimber\Service\Service;
use GroffTech\PhoenixTimber\Config\ConfigService;

/**
 * Theme supports class.
 */
class ThemeSupports extends Service {

    /**
     * The ConfigService class
     *
     * @var ConfigService
     */
    private $config_service;

    /**
     * Constructor.
     *
     * @param ConfigService $config_service The config service.
     */
    public function __construct( ConfigService $config_service ) {
        $this->config_service = $config_service;
    }

	/**
     * Bootstrap the class.
     *
     * Overrides the run method of the parent class.
     *
     * @since 1.0.0
     *
     * @return void
     */
	public function run() {
        $configuration_file = __DIR__ . '/Config/Theme.php';

        $this->config_service->load_configuration( $configuration_file );
        $this->add_theme_support();
	}

	/**
     * Registers hooks.
     *
     * @since 1.0.0
     *
     * @return void
     */
	public function register_hooks() {

	}

	/**
     * Enables theme supports
     *
     * @since 1.0.0
     *
     * @return void
     */
	public function add_theme_support() {
		$feature_name = '';
		$feature_args = array();
		$features = $this->config_service->get_configuration( 'features' );

		foreach ( $features as $feature ) {
            $feature_name = $feature['name'];
            $feature_args = array_shift( $feature );

            if ( '' !== $feature_name && empty( $feature_args ) ) {
                \add_theme_support( $feature_name );
			}

            if ( '' !== $feature_name && ! empty( $feature_args ) ) {
                \add_theme_support( $feature_name, $feature_args );
			}
		}
    }
}
