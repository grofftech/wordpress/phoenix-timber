<?php
/**
 * Page Template
 *
 * @package     GroffTech\PhoenixTimber
 * @since       1.0.0
 * @author      Brett Groff
 * @link        https://grofftech.net
 * @license     GNU General Public License 2.0+
 */

namespace GroffTech\PhoenixTimber;

use Timber;

$timber_post = new Timber\Post();

$context = $timber->get_context();
$context['post'] = $timber_post;

$timber->render( array( "page-{$timber_post->post_name}.twig", 'page.twig' ), $context );
