##################
# Executables
##################
SASS=node_modules/.bin/sass
POST_CSS=node_modules/.bin/postcss
STYLELINT=node_modules/.bin/stylelint
WATCH=node_modules/.bin/watch
TERSER=node_modules/.bin/terser
ESLINT=node_modules/.bin/eslint
BROWSERSYNC=node_modules/.bin/browser-sync
SVGO=node_modules/.bin/svgo
SVGSTORE=node_modules/.bin/svgstore
IMAGEMIN=node_modules/.bin/imagemin

##################
# Configuration
##################
CONFIG_DIR=config
STYLELINT_CONFIG=$(CONFIG_DIR)/stylelintrc.json
STYLELINT_IGNORE=$(CONFIG_DIR)/.stylelintignore
ESLINT_CONFIG=$(CONFIG_DIR)/.eslintrc.json
ESLINT_IGNORE=$(CONFIG_DIR)/.eslintignore
BROWSERSYNC_CONFIG=$(CONFIG_DIR)/browser-sync-config.js
SVGO_CONFIG=$(CONFIG_DIR)/svgo.config.js

##################
# Sources
##################
SOURCE_DIR=assets/src

#######
# CSS
#######
CSS_SOURCE_DIR=$(SOURCE_DIR)/sass

SASS_FILE_STYLE=$(SOURCE_DIR)/sass/style.scss
SASS_FILE_EDITOR=$(SOURCE_DIR)/sass/editor.scss
SASS_FILE_ADMIN=$(SOURCE_DIR)/sass/admin.scss

SASS_FILES=$(SOURCE_DIR)/sass/modules/*

#######
# JS
#######
JS_SOURCE_DIR=$(SOURCE_DIR)/js

# List of JavaScript files to compile (space separated)
JS_FILES="$(JS_SOURCE_DIR)/theme.js"

#######
# SVG
#######
SVG_SOURCE_DIR=$(SOURCE_DIR)/svg

#########
# Images
#########
IMAGE_FILES_JPEG=$(SOURCE_DIR)/images/*.jpg
IMAGE_FILES_PNG=$(SOURCE_DIR)/images/*.png

#######################
# Distribution / Build
#######################
DIST_DIR=assets/dist

#######
# CSS
#######
BUILD_CSS_DIR=$(DIST_DIR)/css

# Front end styles
BUILD_CSS_FILE_STYLE=$(BUILD_CSS_DIR)/style.css
BUILD_CSS_FILE_STYLE_MIN=$(BUILD_CSS_DIR)/style.min.css

# Back end editor styles
BUILD_CSS_FILE_EDITOR=$(BUILD_CSS_DIR)/editor.css
BUILD_CSS_FILE_EDITOR_MIN=$(BUILD_CSS_DIR)/editor.min.css

# Admin styles
BUILD_CSS_FILE_ADMIN=$(BUILD_CSS_DIR)/admin.css
BUILD_CSS_FILE_ADMIN_MIN=$(BUILD_CSS_DIR)/admin.min.css

#######
# JS
#######
BUILD_JS_DIR=$(DIST_DIR)/js

#######
# SVG
#######
BUILD_SVG_DIR=$(DIST_DIR)/svg
BUILD_SVG_OPTIMIZED_DIR=$(BUILD_SVG_DIR)/optimized
BUILD_SVG_FILES=$(BUILD_SVG_OPTIMIZED_DIR)/*.svg
BUILD_SVG_SPRITE_FILE=$(BUILD_SVG_DIR)/sprite.svg

#########
# Images
#########
BUILD_IMAGES_DIR=$(DIST_DIR)/images

############
# Languages
############
BUILD_LANGUAGES_DIR=$(DIST_DIR)/languages

####################
# Tasks
####################
all: clean $(BUILD_CSS_FILE_STYLE) $(BUILD_CSS_FILE_EDITOR) $(BUILD_CSS_FILE_ADMIN) js-files svg-files images-jpg images-png

#########
# CSS
#########

# Builds the style sheet for the front end
$(BUILD_CSS_FILE_STYLE): $(BUILD_CSS_DIR) copy-css-deps compile-style minimize-style
	@ echo "Copying '$(BUILD_CSS_FILE_STYLE)' and '$(BUILD_CSS_FILE_STYLE_MIN)' to root directory..."
	@ cp $(BUILD_CSS_FILE_STYLE) .
	@ cp $(BUILD_CSS_FILE_STYLE_MIN) .

# Creates the directory for final CSS
$(BUILD_CSS_DIR):
	@ echo "Creating directory '$(BUILD_CSS_DIR)'..."
	@ mkdir -p $@

# Copies css dependencies from node_modules to proper sass folder for processing
copy-css-deps:
	@ echo "Copying CSS dependencies..."
	@ cp -v node_modules/normalize.css/normalize.css assets/src/sass/modules/base/reset/_normalize.scss

# Compiles Sass for the front end
compile-style: lint-sass
	@ echo "Compiling Sass for '$(SASS_FILE_STYLE)'..."
	@ $(SASS) $(SASS_FILE_STYLE) $(BUILD_CSS_FILE_STYLE) \
		--style=expanded \
		--source-map \
		--embed-source-map \
		--quiet

# Checks styles for formatting
lint-sass:
	@ echo "Linting Sass..."
	@ $(STYLELINT) $(SASS_FILES) \
		--ignore-path $(STYLELINT_IGNORE) \
		--config $(STYLELINT_CONFIG) \

# Minifies the front end styles
minimize-style: autoprefix-style
	@ echo "Minifying '$(BUILD_CSS_FILE_STYLE)'..."
	@ $(POST_CSS) \
		-u cssnano \
		-o $(BUILD_CSS_FILE_STYLE_MIN) \
		$(BUILD_CSS_FILE_STYLE)

# Adds any vendor prefixes based on configuration in package.json for front end styles
autoprefix-style:
	@ echo "Autoprefixing '$(BUILD_CSS_FILE_STYLE)'..."
	@ $(POST_CSS) \
		-u autoprefixer \
		-r $(BUILD_CSS_FILE_STYLE)

# Builds the style sheet for the editor
$(BUILD_CSS_FILE_EDITOR): compile-editor minimize-editor

# Compiles Sass for the editor
compile-editor:
	@ echo "Compiling Sass for '$(SASS_FILE_EDITOR)'..."
	@ $(SASS) $(SASS_FILE_EDITOR) $(BUILD_CSS_FILE_EDITOR) \
		--style=expanded \
		--source-map \
		--quiet

# Minifies the editor styles
minimize-editor: autoprefix-editor
	@echo "Minifying '$(BUILD_CSS_FILE_EDITOR)'..."
	@ $(POST_CSS) \
		-u cssnano \
		-o $(BUILD_CSS_FILE_EDITOR_MIN) \
		$(BUILD_CSS_FILE_EDITOR)

# Adds any vendor prefixes based on configuration in package.json
# for editor styles
autoprefix-editor:
	@ echo "Autoprefixing '$(BUILD_CSS_FILE_EDITOR)'..."
	@ $(POST_CSS) \
		-u autoprefixer \
		-r $(BUILD_CSS_FILE_EDITOR)

# Builds the style sheet for admin
$(BUILD_CSS_FILE_ADMIN): compile-admin minimize-admin

# Compiles Sass for the admin
compile-admin:
	@ echo "Compiling Sass for '$(SASS_FILE_ADMIN)'..."
	@ $(SASS) $(SASS_FILE_ADMIN) $(BUILD_CSS_FILE_ADMIN) \
		--style=expanded \
		--source-map \
		--quiet

# Minifies the admin styles
minimize-admin: autoprefix-admin
	@echo "Minifying '$(BUILD_CSS_FILE_ADMIN)'..."
	@ $(POST_CSS) \
		-u cssnano \
		-o $(BUILD_CSS_FILE_ADMIN_MIN) \
		$(BUILD_CSS_FILE_ADMIN)

# Adds any vendor prefixes based on configuration in package.json
# for admin styles
autoprefix-admin:
	@echo "Autoprefixing '$(BUILD_CSS_FILE_ADMIN)'..."
	@ $(POST_CSS) \
		-u autoprefixer \
		-r $(BUILD_CSS_FILE_ADMIN)

#######
# JS
#######

# Compiles JavaScript files
js-files: lint-js $(BUILD_JS_DIR)
	@echo "Compiling JS Files..."
	@./scripts/compile-js-files.sh $(JS_FILES) $(BUILD_JS_DIR)

# Creates the directory for the final JavaScript
$(BUILD_JS_DIR):
	@ echo "Creating directory '$(BUILD_JS_DIR)'..."
	@ mkdir -p $@

# Checks JavaScript formatting
lint-js:
	@ echo "Linting JS..."
	@ $(ESLINT) \
		--config $(ESLINT_CONFIG) \
		--ignore-path $(ESLINT_IGNORE) \
		$(JS_SOURCE_DIR)

#######
# SVG
#######

# Optimizies and compiles individual SVGs into a sprite
svg-files:
	@ echo "Processing SVGs..."
	@ echo "Creating directory '$(BUILD_SVG_DIR)'..."
	@ mkdir $(BUILD_SVG_DIR)
	@ echo "Creating directory '$(BUILD_SVG_OPTIMIZED_DIR)'..."
	@ mkdir $(BUILD_SVG_OPTIMIZED_DIR)
	@ echo "Optimizing and compiling..."
	@ $(SVGO) \
		-q \
		--config $(SVGO_CONFIG) \
		-f $(SVG_SOURCE_DIR) \
		-o $(BUILD_SVG_OPTIMIZED_DIR) \
	  && \
	  	$(SVGSTORE) \
	  		-o $(BUILD_SVG_SPRITE_FILE) \
			$(BUILD_SVG_FILES)

########
# Images
########

# Optimizes all .jpg images
images-jpg:
	@ echo "Minifying JPEG images..."
	@ $(IMAGEMIN) \
		$(IMAGE_FILES_JPEG) \
		-o $(BUILD_IMAGES_DIR) \
		-p.mozjpeg.quality=80

# Optimizies all .png images
images-png:
	@ echo "Minifying PNG images..."
	@ $(IMAGEMIN) \
		$(IMAGE_FILES_PNG) \
		-o $(BUILD_IMAGES_DIR)

#######
# Other
#######

# Runs browser sync
browser-sync:
	@ $(BROWSERSYNC) start --config $(BROWSERSYNC_CONFIG)

# Deletes everything in the destination directories
clean:
	@ echo "Removing directory '$(DIST_DIR)'..."
	@ rm -rf $(DIST_DIR)

# Run the all task and watches for changes
watch: clean
	@./watch

.PHONY: all clean copy-css-deps compile-style compile-editor compile-admin lint-sass autoprefix-style autoprefix-editor autoprefix-admin minimize-style minimize-editor minimize-admin lint-js browser-sync js-files svg-files images-jpg images-png
