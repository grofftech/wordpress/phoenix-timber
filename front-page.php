<?php
/**
 * Description
 *
 * @package     GroffTech\PhoenixTimber
 * @since       1.0.0
 * @author      Brett Groff
 * @link        https://grofftech.net
 * @license     GNU General Public License 2.0+
 */

namespace GroffTech\PhoenixTimber;

use GroffTech\PhoenixTimber\Dependencies\Timber\Post;

$context = $timber->get_context();
$context['post'] = new Post(); // Static homepage is set to Home.


if ( is_front_page() ) {
    $timber->render( 'front-page.twig', $context );
}
